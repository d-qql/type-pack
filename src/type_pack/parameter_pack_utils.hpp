#ifndef TYPE_PACK_PARAMETER_PACK_UTILS_HPP
#define TYPE_PACK_PARAMETER_PACK_UTILS_HPP

#include <utility>

#include "single_type.hpp"

namespace tp {

namespace details {

template <typename>
struct get_impl;

template <std::size_t... Is>
struct get_impl<std::index_sequence<Is...>> {
    template <typename T>
    [[maybe_unused]] static constexpr T get(decltype(Is, (void *)(0))..., T *, ...);
};

}  // namespace details

/**
 * Checks whether parameter pack contains a specific type
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return true if Ts contains T, false otherwise
 */
template <typename T, typename... Ts>
constexpr bool contains() noexcept {
    return (std::is_same_v<T, Ts> || ...);
}

/**
 * Finds index of a specific type in a parameter pack
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return index of the first element of type T in Ts if it exists, size of Ts otherwise
 */
template <typename T, typename... Ts>
constexpr std::size_t find() noexcept {
    constexpr bool is_true[] = {std::is_same_v<T, Ts>...};
    for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
        if (is_true[i]) {
            return i;
        }
    }
    return sizeof...(Ts);
}

/**
 * Finds index of the first type in a parameter pack satisfying specific criteria
 * @tparam F predicate which returns true for the required element
 * @tparam Ts parameter pack
 * @return index of the first element satisfying F if it exists, size of Ts otherwise
 */
template <template <typename...> typename F, typename... Ts>
constexpr std::size_t find_if() noexcept {
    constexpr bool is_true[] = {F<Ts>::value...};
    for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
        if (is_true[i]) {
            return i;
        }
    }
    return sizeof...(Ts);
}

/**
 * Returns the number of types in a parameter pack same as a specific type
 * @tparam T type to search for
 * @tparam Ts parameter pack
 * @return number of types in Ts same as T
 */
template <typename T, typename... Ts>
constexpr std::size_t count() noexcept {
    constexpr bool is_true[] = {std::is_same_v<T, Ts>...};
    std::size_t c            = 0;
    for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
        if (is_true[i]) {
            ++c;
        }
    }
    return c;
}

/**
 * Returns the number of types in a parameter pack satisfying specific criteria
 * @tparam F predicate which returns true for the required element
 * @tparam Ts parameter pack
 * @return number of types in Ts satisfying F
 */
template <template <typename...> typename F, typename... Ts>
constexpr std::size_t count_if() noexcept {
    constexpr bool is_true[] = {F<Ts>::value...};
    std::size_t c            = 0;
    for (std::size_t i = 0; i < sizeof...(Ts); ++i) {
        if (is_true[i]) {
            ++c;
        }
    }
    return c;
}

/**
 * Checks whether parameter pack contains unique elements (exactly one entry of each type)
 * @tparam Ts parameter pack
 * @return true if Ts is unique, false otherwise
 */
template <typename... Ts>
constexpr bool unique() noexcept {
    return ((count<Ts, Ts...>() == 1) && ...);
}

/**
 * Gets type from parameter pack using index
 * @tparam I index of the required type
 * @tparam Ts parameter pack
 * @return struct single_type, containing required type T
 */
template <std::size_t I, typename... Ts>
constexpr auto get() noexcept {
    return single_type<decltype(details::get_impl<std::make_index_sequence<I>>::get((Ts *)(0)...))>{};
}

template <std::size_t I, typename... Ts>
using get_t = typename decltype(get<I, Ts...>())::type;

}  // namespace tp

#endif  // TYPE_PACK_PARAMETER_PACK_UTILS_HPP